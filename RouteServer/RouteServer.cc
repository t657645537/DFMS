/*************************************************************************
	> File Name: RouteServer/RouteServer.cc
	> Author: tianzuojian
	> Mail: t657645537@126.com 
	> Created Time: 2015年07月30日 星期四 15时43分58秒
 ************************************************************************/

#include <iostream>
#include <xLib/TimeHandler.hh>
#include "PluginManager/PluginBase.hh"
using namespace std;

class RouteServer :
	public PluginBase
{
	public:
		RouteServer(int thePort):
			PluginBase(thePort,"RouteServer"),
			myTimeHandler(myIoService)
		{
				
		};
		virtual void print()
		{
			LOG(INFO)<<"[RouteServer::print]";
		}
		virtual void start()
		{
			// myTimeHandler.timeOut(5,boost::bind(&RouteServer::print,this));
			cout << "[RouteServer::start]" << endl;
			PluginBase::start();
			// myIoService.run();
		};
		virtual void stop()
		{
			PluginBase::stop();
		};
		virtual void networkCallback(const string& theUUID, const string& theStr)
		{
			PluginBase::networkCallback(theUUID,theStr);
		};
	private:
		TimeHandler myTimeHandler;
		boost::asio::io_service myIoService;
};

extern "C" RouteServer* create(int thePort)
{
	return new RouteServer(thePort);
}
extern "C" void destroy(PluginBase* thePluginBase)
{
	delete thePluginBase;
}
